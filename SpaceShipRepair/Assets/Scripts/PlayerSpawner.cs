﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerSpawner : MonoBehaviour
{
    public Transform spawnPoint;
    public UnityEvent OnSpawn;
    public UnityEventInt OnSpawnTimer;

    public void SetTimer()
    {
        StartCoroutine(RunSpawnTimer());
    }

    IEnumerator RunSpawnTimer()
    {
        int timer = 5;
        while(timer > 0)
        {
            OnSpawnTimer.Invoke(timer);
            yield return new WaitForSeconds(1f);
            timer -= 1;
            
        }
        Spawn();
    }

    public void Spawn()
    {
        GetComponent<Rigidbody>().position = spawnPoint.position;
        OnSpawn.Invoke();
    }

    public void PrintSpawnTimer(int time)
    {
        print("Spawn " + gameObject.name + " in " + time);
    }
}
