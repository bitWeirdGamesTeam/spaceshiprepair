﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarParticleBehaviour : MonoBehaviour
{
    public ParticleSystem starParticles;
    public ThrusterBehaviour[] thrusters;

    private int activeThrusters = 0;

    // Update is called once per frame
    void Update()
    {
        CheckThrusters();
    }

    public void CheckThrusters()
    {
        int activeThrusterCount = 0;
        for(int i = 0; i < thrusters.Length; i++)
        {
            if (thrusters[i].CheckThrusterActivity())
                activeThrusterCount++;
        }

        if(activeThrusterCount != activeThrusters)
        {
            activeThrusters = activeThrusterCount;
            SetParticleSpeed((float)activeThrusters / (float)thrusters.Length);
        }
    }

    void SetParticleSpeed(float speed)
    {
        ParticleSystem.MainModule main = starParticles.main;
        main.simulationSpeed = speed;
    }
}
