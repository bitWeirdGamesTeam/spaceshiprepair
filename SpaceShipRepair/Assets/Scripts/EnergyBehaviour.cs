﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnergyBehaviour : MonoBehaviour, IEnergized
{
    public bool IsEnergized { get; set; }
    public UnityEvent OnEnergyOn;
    public UnityEvent OnEnergyOff;

    public void EnergyOn()
    {
        IsEnergized = true;
        OnEnergyOn.Invoke();
    }

    public void EnergyOff()
    {
        IsEnergized = false;
        OnEnergyOff.Invoke();
    }
}
