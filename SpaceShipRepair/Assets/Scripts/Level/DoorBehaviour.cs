﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DoorBehaviour : AbstractUsable {

    public Transform dooframeTransform;
    public Rigidbody body;
    public NavMeshObstacle navObstacle;

    public float maxAngle = 90f;
    public float movementDuration = 0.5f;

    public AudioClip openSound;
    public AudioClip closeSound;
    public AudioSource audioSource;

    private Quaternion startRotation = Quaternion.identity;
    private Quaternion endRotation = Quaternion.identity;
    private Quaternion openCWRotation;
    private Quaternion openCCWRotation;
    private float translation = 0f;
    private float translationSpeed = 0f;

    private State state = State.SHUT;
    private enum State
    {
        SHUT,
        OPEN,
        OPENING,
        CLOSING,
    }

	// Use this for initialization
	void Start () {
        openCWRotation = Quaternion.Euler(0f, maxAngle, 0f);
        openCCWRotation = Quaternion.Euler(0f, -maxAngle, 0f);
    }
	
	// Update is called once per frame
	void Update () {
        if(state == State.OPENING || state == State.CLOSING)
        {
            translation += translationSpeed * Time.deltaTime;
            Quaternion currentRot;

            if (translation < 1f)
                currentRot = Quaternion.Lerp(startRotation, endRotation, translation);
            else
            {
                currentRot = endRotation;
                if (state == State.OPENING)
                {
                    state = State.OPEN;
                    navObstacle.enabled = true;
                    navObstacle.carving = true;
                }
                else
                {
                    PlaySound(closeSound);
                    state = State.SHUT;
                }
            }

            body.MoveRotation(currentRot);
        }
    }

    public void Open(Transform userTransform)
    {
        if (state == State.OPENING || state == State.OPEN)
            return;

        startRotation = body.rotation;
        Vector3 userDirection = (userTransform.position - dooframeTransform.position).normalized;
        if (Vector3.Dot(dooframeTransform.forward, userDirection) > 0f)
            endRotation = dooframeTransform.rotation * openCWRotation;
        else
            endRotation = dooframeTransform.rotation * openCCWRotation;

        float deltaAngle = Quaternion.Angle(body.rotation, endRotation);
        translationSpeed = (maxAngle / deltaAngle) * (1f/movementDuration);
        translation = 0f;

        PlaySound(openSound);

        state = State.OPENING;
    }

    public void Close(Transform userTransform)
    {
        if (state == State.CLOSING || state == State.SHUT)
            return;

        startRotation = body.rotation;
        endRotation = dooframeTransform.rotation;

        float deltaAngle = Quaternion.Angle(body.rotation, endRotation);
        translationSpeed = (maxAngle / deltaAngle) * (1f/movementDuration);
        translation = 0f;

        navObstacle.enabled = false;
        navObstacle.carving = false;

        state = State.CLOSING;
    }

    private void PlaySound(AudioClip clip)
    {
        if(audioSource != null && clip != null)
            audioSource.PlayOneShot(clip);
    }

    protected override void OnActivate(Transform user)
    {
        Open(user);
    }

    protected override void OnDeactivate(Transform user)
    {
        Close(user);
    }
}
