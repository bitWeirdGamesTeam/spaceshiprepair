﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDoorOpener : MonoBehaviour {

    public DoorBehaviour doorBehaviour;
    public Transform doorFrameTransform;
    //public Collider enterTrigger;
    public LayerMask npcLayers;

    private float timer = 0f;

    private bool CheckLayer(int layer)
    {
        return ((1 << layer) & npcLayers.value) > 0;
    }

    private void AttemptOpen(GameObject user)
    {
        NPCDoorUsage doorUsage = user.GetComponent<NPCDoorUsage>();
        if (doorUsage != null && doorUsage.openDoors == true)
        {
            Vector3 userLocalPos = doorFrameTransform.InverseTransformPoint(user.transform.position);

            if(userLocalPos.z < 0f)
            {
                if (Vector3.Dot(doorFrameTransform.forward, user.transform.forward) > -0.1f)
                    doorBehaviour.Open(user.transform);
            }
            else
            {
                if (Vector3.Dot(doorFrameTransform.forward, user.transform.forward) < 0.1f)
                    doorBehaviour.Open(user.transform);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(CheckLayer(other.attachedRigidbody.gameObject.layer))
        {
            AttemptOpen(other.attachedRigidbody.gameObject);
            timer = 1f;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (CheckLayer(other.attachedRigidbody.gameObject.layer))
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                AttemptOpen(other.attachedRigidbody.gameObject);
                timer = 1f;
            }
        }
    }
}
