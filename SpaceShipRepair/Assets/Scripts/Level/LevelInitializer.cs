﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelInitializer : MonoBehaviour
{
    public UnityEvent OnLevelInitialize;
    public List<GameObject> players; 

    private void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        int pCount = PlayerPrefs.GetInt("pCount");
        for(int i=0; i<players.Count; i++)
        {
            if (i < pCount)
                players[i].SetActive(true);
            else
                players[i].SetActive(false);
        }
        OnLevelInitialize.Invoke();
    }

    public void SetPlayerCount(int p)
    {
        PlayerPrefs.SetInt("pCount", p);
    }

    public void LoadPlayScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void LoadMenuScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
