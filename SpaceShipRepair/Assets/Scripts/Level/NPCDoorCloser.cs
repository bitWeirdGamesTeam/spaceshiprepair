﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDoorCloser : MonoBehaviour {

    public DoorBehaviour doorBehaviour;
    public Transform doorFrameTransform;
    //public Collider exitTrigger;
    public LayerMask npcLayers;

    private void AttemptCloseDoor(GameObject user)
    {
        NPCDoorUsage doorUsage = user.GetComponent<NPCDoorUsage>();
        if (doorUsage != null && doorUsage.closeDoors == true)
        {
            Vector3 userLocalPos = doorFrameTransform.InverseTransformPoint(user.transform.position);

            if (userLocalPos.z < 0f)
            {
                if (Vector3.Dot(doorFrameTransform.forward, user.transform.forward) < 0.1f)
                    doorBehaviour.Close(user.transform);
            }
            else
            {
                if (Vector3.Dot(doorFrameTransform.forward, user.transform.forward) > -0.1f)
                {
                    doorBehaviour.Close(user.transform);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (((1 << other.attachedRigidbody.gameObject.layer) & npcLayers.value) > 0)
        {
            AttemptCloseDoor(other.attachedRigidbody.gameObject);
        }
    }
}
