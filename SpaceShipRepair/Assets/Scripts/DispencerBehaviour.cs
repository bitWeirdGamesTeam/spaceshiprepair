﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispencerBehaviour : MonoBehaviour
{
    public GameObject itemPrefab;

    public void Use(Transform user)
    {
        ObjectManipulator om = user.GetComponent<ObjectManipulator>();
        if (om.IsCarrying)
            return;
        else
        {
            GameObject go = Instantiate(itemPrefab);
            go.transform.position = transform.position + new Vector3(-0.3f, 0.5f);
            om.PickUp(go);
        }
    }
}
