﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractHealth : MonoBehaviour, IIsDead {

    public int maxHealth = 1;
    public IntReference health;

    public bool IsDead
    {
        get { return health <= 0; }
    }

    private void OnEnable()
    {
        ResetHealth();
    }

    public void ResetHealth()
    {
        health.Value = maxHealth;
    }

    public void ChangeHealth(int amount)
    {
        SetHealth(health.Value + amount);
    }

    public void SetHealth(int newHealth)
    {
        int oldHealth = health.Value;
        health.Value = Mathf.Clamp(newHealth, 0, maxHealth);

        if (newHealth > 0)
        {
            if (newHealth < oldHealth)
                ReactDamage(oldHealth - newHealth);
            else if (oldHealth <= 0)
                ReactRevive();
        }
        else if (oldHealth > 0)
            ReactDeath();
    
        ReactHealthChange();
    }

    public void Revive()
    {
        if (health.Value <= 0)
        {
            health.Value = maxHealth;
            ReactRevive();
        }
        else
            health.Value = maxHealth;
    }

    public void ProcessDamage(int damage)
    {
        if(health > 0)
        {
            health.Value -= damage;

            if(health.Value <= 0)
            {
                health.Value = 0;
                ReactDeath();
            }
            else
            {
                ReactDamage(damage);
            }

            ReactHealthChange();
        }
    }

    public abstract void ReactDamage(int damage);
    public abstract void ReactDeath();
    public abstract void ReactRevive();
    public abstract void ReactHealthChange();
}
