﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthComboEvent : AbstractHealth {

    public UnityEventInt2 OnDamage;
    public UnityEvent OnDeath;
    public UnityEvent OnRevive;
    public UnityEventInt OnHealthChange;

    public GameEvent onDamage;
    public GameEventGO onDeath;
    public GameEvent onRevive;
    public GameEvent onHealthChange;

    public override void ReactDamage(int damage)
    {
        if (OnDamage != null)
            OnDamage.Invoke(health.Value, damage);

        if (onDamage != null)
            onDamage.Trigger();
    }

    public override void ReactDeath()
    {
        if (OnDeath != null)
            OnDeath.Invoke();

        if (onDeath != null)
            onDeath.Trigger(gameObject);
    }

    public override void ReactHealthChange()
    {
        if (OnHealthChange != null)
            OnHealthChange.Invoke(health.Value);

        if (onHealthChange != null)
            onHealthChange.Trigger();
    }

    public override void ReactRevive()
    {
        if (OnRevive != null)
            OnRevive.Invoke();

        if (onRevive != null)
            onRevive.Trigger();
    }
}
