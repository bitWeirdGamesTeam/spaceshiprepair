﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageApplier : MonoBehaviour {

    public int damage = 1;
    public float damageRadius = 5f;
    public LayerMask damageLayers;

    public void ApplyDamage(Collider col)
    {
        if(((1 << col.gameObject.layer) & damageLayers.value) > 0)
        {
            AbstractHealth health = col.GetComponent<AbstractHealth>();
            if(health != null)
                health.ProcessDamage(damage);
        }
    }

    public void ApplyDamageProportional(Collider col)
    {
        Debug.Log("Apply damage");
        if (damageRadius > 0 && ((1 << col.gameObject.layer) & damageLayers.value) > 0)
        {
            AbstractHealth health = col.GetComponent<AbstractHealth>();
            if (health != null)
            {
                float distance = Vector3.Distance(transform.position, col.transform.position);

                if (distance < damageRadius)
                {
                    //Damage multiplier grows inverse to distance
                    float damageMultiplier = (float)(1f - distance / damageRadius);
                    int relativeDamage = Mathf.CeilToInt(damageMultiplier * (float)damage);

                    health.ProcessDamage(relativeDamage);
                }
            }
        }
    }
}
