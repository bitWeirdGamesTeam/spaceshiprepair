﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IIsDead {

    bool IsDead { get; }
}
