﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudioAtPoint : MonoBehaviour
{
    public AudioClip clip;

    public void PlayAtPosition(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(clip, position);
    }

    public void PlayClipHere()
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
    }
}
