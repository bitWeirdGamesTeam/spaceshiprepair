﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnergized 
{
    bool IsEnergized { get; set; }
}
