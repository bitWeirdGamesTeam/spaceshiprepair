﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManager : MonoBehaviour
{
    public GameObject asteroidPrefab;
    //public float spawnDelay = 5f;
    public AnimationCurve spawnerCurve;
    public HealthBehaviour shipHealth;
    public HealthBehaviour shieldHealth;
    public AudioSource effectsSource;

    private void OnEnable()
    {
        StartAsteroids();
    }

    public void StartAsteroids()
    {
        StartCoroutine(SpawnAsteroids());
    }

    IEnumerator SpawnAsteroids()
    {
        float timer = 0f;
        float startTime = Time.realtimeSinceStartup;
        while (true)
        {
            timer = Time.realtimeSinceStartup - startTime;
            float next = spawnerCurve.Evaluate(timer);
            yield return new WaitForSeconds(next + Random.value * 4f);
            SpawnAsteroid();
            //print("Spawn "+ timer + ", " + next);
        }
    }

    public void SpawnAsteroid()
    {
        GameObject go = Instantiate(asteroidPrefab);
        go.transform.position = transform.position + new Vector3(0, 0, Random.Range(-2, 2));
        //go.GetComponent<Mover>().StartMovement();
        go.GetComponent<Rigidbody>().AddForce(transform.forward * 4f, ForceMode.VelocityChange);
        go.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.value, Random.value, Random.value) * 10f, ForceMode.VelocityChange);
        go.GetComponent<InpactChecker>().asteroidManager = this;
    }
}
