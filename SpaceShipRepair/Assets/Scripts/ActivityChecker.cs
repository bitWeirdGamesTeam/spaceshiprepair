﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivityChecker : MonoBehaviour, IEnergized
{
    public bool IsEnergized { get; set; } = true;
    public HealthBehaviour health;
    public float checkTick = 0.1f;

    public UnityEvent Activate;
    public UnityEvent Deactivate;

    public bool CheckActivity()
    {
        return IsEnergized && health.Health > 0;
    }

    private void OnEnable()
    {
        StartChecking();
    }

    public void StartChecking()
    {
        StartCoroutine(DoChecking());
    }

    IEnumerator DoChecking()
    {
        bool old = false;
        while (true)
        {
            bool isOn = CheckActivity();
            if (isOn && old != isOn)
                Activate.Invoke();
            if (!isOn && old != isOn)
                Deactivate.Invoke();

            old = isOn;

            yield return new WaitForSeconds(checkTick);
        }
    }
}
