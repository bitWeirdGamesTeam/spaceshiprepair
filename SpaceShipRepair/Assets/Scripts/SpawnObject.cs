﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    public GameObject prefab;
    public Vector3 offset;
    
    public void Spawn()
    {
        GameObject go = Instantiate(prefab);
        go.transform.position = transform.position + offset;
    }
}
