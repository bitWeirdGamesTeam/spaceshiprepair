﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Mover : MonoBehaviour
{
    public Rigidbody rig;
    public Vector3 moveVector;
    public float speed = 1f;
    public UnityEvent OnCollision;
    public bool IsMoving { get; private set; }


    public void StartMovement()
    {
        StartCoroutine(DoMoving());
    }

    IEnumerator DoMoving()
    {
        IsMoving = true;
        while(IsMoving)
        {
            Move(moveVector * Time.deltaTime * speed);
            yield return null;
        }

    }

    public void Move(Vector3 vector)
    {
        rig.MovePosition(rig.position + vector);
    }

    private void OnCollisionEnter(Collision collision)
    {
        IsMoving = false;
        OnCollision.Invoke(); 
    }

    [ContextMenu("Set Forward")]
    public void SetForward()
    {
        moveVector = transform.forward;
    }
}
