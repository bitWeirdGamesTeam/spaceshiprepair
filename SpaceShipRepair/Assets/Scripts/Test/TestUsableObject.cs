﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUsableObject : MonoBehaviour
{
    public Color[] colors;
    public Renderer rend;

    public int currentColorIndex = 0;

    public void CycleColors()
    {
        currentColorIndex++;
        if (currentColorIndex >= colors.Length)
            currentColorIndex = 0;
        rend.material.color = colors[currentColorIndex];
    }
}
