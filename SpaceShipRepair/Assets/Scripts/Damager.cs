﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    public int damagePerTick = -1;
    public HealthBehaviour healthBehaviour;

    public void DoDamage()
    {
        healthBehaviour.ChangeHealth(damagePerTick);
    }
}
