﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InpactChecker : MonoBehaviour
{
    public AsteroidManager asteroidManager;
    public int damage = -20;
    public UnityEvent OnCollision;
    public AudioClip shieldHitSound;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9)
        {
            asteroidManager.shieldHealth.ChangeHealth(damage);
            if (shieldHitSound != null)
                asteroidManager.effectsSource.PlayOneShot(shieldHitSound, 0.35f);
        }
        else
            asteroidManager.shipHealth.ChangeHealth(damage);

        OnCollision.Invoke();
    }
}
