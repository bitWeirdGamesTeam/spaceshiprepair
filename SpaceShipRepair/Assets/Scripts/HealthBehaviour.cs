﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthBehaviour : MonoBehaviour
{
    public int maxHealth = 100;
    public int Health { get; private set; }
    public UnityEventInt OnHealthChanged;
    public UnityEventFloat OnHealthRatioChanged;
    public UnityEventInt OnReceivedDamage;
    public UnityEvent OnDeath;

    private void Start()
    {
        Health = maxHealth;
        OnHealthChanged.Invoke(Health);
        OnHealthRatioChanged.Invoke((float)Health / maxHealth);
    }

    public void ChangeHealth(int delta)
    {
        int oldHealth = Health;
        Health += delta;
        Health = Mathf.Clamp(Health, 0, maxHealth);
        OnHealthChanged.Invoke(Health);
        OnHealthRatioChanged.Invoke((float)Health/maxHealth);
        if (delta < 0)
            OnReceivedDamage.Invoke(delta);
        if (Health == 0 && oldHealth != 0)
            OnDeath.Invoke();
    }
    
    public void PrintHealth()
    {
        print("Health: " + Health);
    }

    public void PrintDeath()
    {
        print(string.Format("{0} Death", gameObject.name));
    }

    [ContextMenu("Kill")]
    public void Kill()
    {
        ChangeHealth(-maxHealth);
    }

    
    public void MaxHealth()
    {
        ChangeHealth(maxHealth);
    }
}
