﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {

    public Camera cam;
    public Transform target;

    public float sensitivity = 0.2f;

    public float slidingAverageCount = 1f;

    public float maxDistance = 20f;
    public float minDistance = 3f;

    public Vector2 offset = Vector2.zero;

    public float clearanceRadius = 0.5f;
    public LayerMask solidLayers;

    public Vector3 Rotation
    {
        get
        {
            UpdateRotation();
            return rotationSmoothed;
        }
    }

    private float distance = 10f;
    private Vector3 offsetPosition = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private Vector3 rotationSmoothed = Vector3.zero;
    private Vector3 right = Vector3.zero;
    private bool calculatedRotation = false;

	// Use this for initialization
	void Start ()
    {
        distance = maxDistance;

        if (target != null)
        {
            cam.transform.LookAt(target);
            cam.transform.position = target.position - cam.transform.forward * distance;
            offsetPosition = cam.transform.position - target.position;
            rotation = cam.transform.rotation.eulerAngles;
            rotationSmoothed = rotation;
            right = Quaternion.Euler(rotationSmoothed + new Vector3(0f, 90f, 0f)) * Vector3.forward;
        }

        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update ()
    {
        if(target != null)
        {
            if (calculatedRotation == false)
                UpdateRotation();

            distance += Input.mouseScrollDelta.y * -1f;
            distance = Mathf.Clamp(distance, minDistance, maxDistance);

            offsetPosition = Quaternion.Euler(rotationSmoothed) * -Vector3.forward * distance;

            RaycastHit rayHit = new RaycastHit();
            if(Physics.SphereCast(target.position + right * offset.x + Vector3.up * offset.y, clearanceRadius, offsetPosition.normalized, out rayHit, distance, solidLayers.value))
            {
                cam.transform.position = rayHit.point + rayHit.normal * clearanceRadius;//offsetPosition.normalized * clearanceRadius;
                cam.transform.LookAt(target.position + right * offset.x + Vector3.up * offset.y);
            }
            else
            {
                cam.transform.position = target.position + offsetPosition + right * offset.x + Vector3.up * offset.y;
                cam.transform.LookAt(target.position + right * offset.x + Vector3.up * offset.y);
                //cam.transform.position += cam.transform.right * offset.x + Vector3.up * offset.y;
            }
        }

        /*
        if ((cam.transform.position - target.position + (right * offset.x + Vector3.up * offset.y)).magnitude < 0.5f)
        {
            Renderer rend = target.parent.GetComponentInChildren<SkinnedMeshRenderer>();
            rend.enabled = false;
            Debug.Log(rend.gameObject.name);
        }
        else
        {
            Renderer rend = target.parent.GetComponentInChildren<SkinnedMeshRenderer>();
            rend.enabled = true;
            Debug.Log("Visible");
        }
        */
        calculatedRotation = false;
    }

    void UpdateRotation()
    {
        rotation.y += Input.GetAxis("Mouse X") * sensitivity;
        rotation.x += Input.GetAxis("Mouse Y") * -sensitivity;
        rotation.x = Mathf.Clamp(rotation.x, -89f, 89f);

        rotationSmoothed = (rotationSmoothed * (slidingAverageCount - 1) + rotation) / slidingAverageCount;

        right = Quaternion.Euler(rotationSmoothed + new Vector3(0f, 90f, 0f)) * Vector3.forward;

        calculatedRotation = true;
    }
}
