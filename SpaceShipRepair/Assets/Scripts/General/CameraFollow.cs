﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public CameraTargetsList cameraTargetsList;
    public Vector3 offset;
    public Bounds movementArea;

    public Transform audioListener;

    public float dampTime = 0.20f;
	private Vector3 velocity = Vector3.one;
    private Vector3 destination;

    private void Start()
    {
        destination = transform.position;
    }

    void LateUpdate () 
	{
		if (cameraTargetsList.HasTargets)
		{
            UpdateDestination();
		}
        UpdateCameraPosition();
	}

    Vector3 ClampPosition(Vector3 position)
    {
        position.x = Mathf.Clamp(position.x, movementArea.min.x, movementArea.max.x);
        position.y = Mathf.Clamp(position.y, movementArea.min.y, movementArea.max.y);
        position.z = Mathf.Clamp(position.z, movementArea.min.z, movementArea.max.z);
        return position;
    }

    void UpdateDestination()
    {
        destination = cameraTargetsList.AveragePosition() + offset;

        destination = ClampPosition(destination);
    }

	void UpdateCameraPosition()
	{
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

        if (audioListener != null)
            audioListener.position = cameraTargetsList.AveragePosition();
	}

    [ContextMenu("Init position")]
    void InitPosition()
    {
        if(cameraTargetsList.HasTargets)
        {
            transform.position = ClampPosition(cameraTargetsList.AveragePosition() + offset);
        }
    }

    [ContextMenu("Write position")]
    void WritePos()
    {
        if (cameraTargetsList.HasTargets)
        {
            offset = transform.position - cameraTargetsList.AveragePosition();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(movementArea.center, movementArea.size);
    }
}
