﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.Events
{
    [System.Serializable]
    public class UnityEventInt : UnityEvent<int> { }

    [System.Serializable]
    public class UnityEventInt2 : UnityEvent<int, int> { }

    [System.Serializable]
    public class UnityEventCollider : UnityEvent<Collider> { }

    [System.Serializable]
    public class UnityEventFloat : UnityEvent<float> { }

    [System.Serializable]
    public class UnityEventBool : UnityEvent<bool> { }

    [System.Serializable]
    public class UnityEventArray : UnityEvent<object[]> { }

    [System.Serializable]
    public class UnityEventGO : UnityEvent<GameObject> { }

    [System.Serializable]
    public class UnityEventVector3 : UnityEvent<Vector3> { }

    [System.Serializable]
    public class UnityEventVector3Float : UnityEvent<Vector3, float> { }

    [System.Serializable]
    public class UnityEventStorage : UnityEvent<string, int, GameObject> { }

    [System.Serializable]
    public class UnityEventCollisionInfo : UnityEvent<CollisionInfo> { }

    [System.Serializable]
    public class UnityEventCollisionInfoFloat : UnityEvent<CollisionInfo, float> { }

    [System.Serializable]
    public class UnityEventItemType : UnityEvent<ItemType> { }

    [System.Serializable]
    public class UnityEventTransform : UnityEvent<Transform> { }
}
