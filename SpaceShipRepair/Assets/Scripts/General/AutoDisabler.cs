﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisabler : MonoBehaviour {

    public float lifeTime = 3f;

    private void OnEnable()
    {
        StartCoroutine(OnDoDisable());
    }

    public void Disable()
    {
        StopCoroutine(OnDoDisable());
        gameObject.SetActive(false);
    }

    IEnumerator OnDoDisable()
    {
        yield return new WaitForSeconds(lifeTime);

        gameObject.SetActive(false);
    }
}
