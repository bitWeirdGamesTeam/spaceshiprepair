﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionInfo
{
    public Transform collidedTransform = null;
    public Vector3 throwOrigin = Vector3.zero;
    public float relativeCollisionSpeed = 0f;
    public float timeStamp = 0f;
}

public class CollisionSensor : MonoBehaviour {

    public LayerMask detectLayers;
    public float detectSpeed = 10f;
    public float infoLifeTime = 0.1f;

    private CollisionInfo storedCollision = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool CheckForCollisions(out CollisionInfo collisionInfo)
    {
        collisionInfo = null;

        if(storedCollision != null)
        {
            if (Time.time < storedCollision.timeStamp + infoLifeTime)
            {
                collisionInfo = storedCollision;

                return true;
            }
            else
            {
                storedCollision = null;
            }
        }
        return false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(((1 << collision.collider.gameObject.layer) & detectLayers.value) > 0)
        {
            float speed = collision.relativeVelocity.magnitude;
            if (speed > detectSpeed)
            {
                if(storedCollision == null)
                {
                    storedCollision = new CollisionInfo();
                }
                storedCollision.collidedTransform = collision.collider.attachedRigidbody.transform;

                PickUpObject manipulable = collision.collider.attachedRigidbody.GetComponent<PickUpObject>();
                if (manipulable != null)
                {
                    storedCollision.throwOrigin = manipulable.ThrowOrigin;
                }
                else
                    storedCollision.throwOrigin = storedCollision.collidedTransform.position;

                storedCollision.relativeCollisionSpeed = speed;
                storedCollision.timeStamp = Time.time;
            }
        }
    }
}
