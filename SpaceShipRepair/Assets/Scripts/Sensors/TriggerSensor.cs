﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerSensor : MonoBehaviour {

    public LayerMask triggerLayers;
    public UnityEventCollider OnTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if(((1 << other.gameObject.layer) & triggerLayers.value) > 0)
            if (OnTrigger != null)
                OnTrigger.Invoke(other);
    }
}
