﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public FloatReference throwPower;

    public ObjectManipulator manipulator;
    public PlayerManipulatorControl playerManipulatorControl;

    public string useButtonName = "";

    private Vector2 movementAxis = Vector2.zero;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        UpdateThrow();
        UpdateObjectUser();
	}

    void UpdateThrow()
    {
        throwPower.Value = playerManipulatorControl.Charge;
    }

    void UpdateObjectUser()
    {
        if (Input.GetButtonDown(useButtonName))
            manipulator.Use();
    }
}
