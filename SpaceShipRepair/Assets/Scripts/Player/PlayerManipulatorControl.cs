﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManipulatorControl : AbstractChargeControl
{
    public ObjectManipulator manipulator;

    public float maxThrowSpeed = 10f;

    [LayerSelector]
    public int holdLayer;

    public string holdAttribute = "";

    private bool canDropOrThrow = false;

    protected override void Update()
    {
        UpdateButton();
        if (canDropOrThrow == true)
            UpdateCharge();
    }

    protected override void OnButtonDown()
    {
        if (manipulator.IsCarrying == false)
        {
            manipulator.PickUp();
        }
    }

    protected override void OnButtonUp()
    {
        if (manipulator.IsCarrying == true)
            canDropOrThrow = true;
    }

    protected override void OnClick()
    {
        Drop();
    }

    protected override void OnChargeRelease()
    {
        if (manipulator.IsCarrying == true)
        {
            manipulator.Throw(charge * maxThrowSpeed);
            canDropOrThrow = false;
        }
    }

    public void Drop()
    {
        if (manipulator.IsCarrying == true)
        {
            manipulator.Drop();
            canDropOrThrow = false;
            ResetCharge();
        }
    }
}
