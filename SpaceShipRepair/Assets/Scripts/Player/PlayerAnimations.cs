﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    public Animator animator;
    public SimpleCharacterMovement3D movement;
    public ObjectManipulator manipulator;

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("moving", movement.ControlAxis.magnitude > 0.05f);
        animator.SetBool("carrying", manipulator.IsCarrying);
        animator.SetFloat("speed", movement.ControlAxis.magnitude);
    }
}
