﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCharacterMovement3D : MonoBehaviour {

    public float walkingSpeed = 2f;
    public float runningSpeed = 4f;
    public float acceleration = 5f;
    public float deceleration = 5f;
    public float dashSpeed = 10f;
    public float dashRecoveryDelay = 1f;

    private Vector2 controlAxis = Vector2.zero;
    private bool run = false;
    private bool dash = false;
    private bool dashAvailable = true;

    private float dashRecoveryTimer = 0f;

    private Vector3 referenceForward = Vector3.forward;
    private Vector3 referenceRight = Vector3.right;

    private Rigidbody rigidBody;

    public Vector3 ControlAxis
    {
        set { controlAxis = value; }
        get { return controlAxis; }
    }

    public bool Run
    {
        set { run = value; }
    }

    public bool Dash
    {
        set { dash = value; }
    }

    public float SetReferenceRotation
    {
        set
        {
            Quaternion refRotation = Quaternion.Euler(new Vector3(0f, value));
            referenceForward = refRotation * Vector3.forward;
            referenceRight = refRotation * Vector3.right;
        }
    }

    public Vector3 Velocity
    {
        get { return rigidBody.velocity; }
    }

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void FixedUpdate()
    {
        UpdateMovement();
    }

    public void UpdateControls(Vector3 controlAxis, bool run)
    {
        this.controlAxis = controlAxis;
        this.run = run;
    }

    void UpdateMovement()
    {
        Vector3 velocity = rigidBody.velocity;
        Vector3 targetVelocity = Vector3.zero;
        Vector3 deltaVelocity = Vector3.zero;
        float maxDelta = 0f;
        bool stopped = false;

        if (dashRecoveryTimer < dashRecoveryDelay)
        {
            dashRecoveryTimer += Time.deltaTime;
        }

        if (dash == false && dashRecoveryTimer >= dashRecoveryDelay && dashAvailable == false)
            dashAvailable = true;

        if(dash && dashAvailable)
        {
            dashAvailable = false;
            dashRecoveryTimer = 0f;

            targetVelocity = referenceForward * controlAxis.y + referenceRight * controlAxis.x;
            targetVelocity *= dashSpeed;
            maxDelta = dashSpeed;
        }
        else if (controlAxis.sqrMagnitude > 0.1f)
        {
            //Accelerate when controlling
            targetVelocity = referenceForward * controlAxis.y + referenceRight * controlAxis.x;

            //Run if pressed 'run', always walk when crouched
            if (run == false)
                targetVelocity *= walkingSpeed;
            else
                targetVelocity *= runningSpeed;

            maxDelta = acceleration * Time.deltaTime;
        }
        else
        {
            maxDelta = deceleration * Time.deltaTime;
            stopped = true;
        }

        //Only change velocity if target speed is greater and we're not stopped, so we don't slow down when fex. flying fast and steering in the same direction
        float relativeSpeed = RelativeVectorMagnitude(velocity, targetVelocity);

        if (targetVelocity.magnitude > relativeSpeed || stopped == true)
        {
            deltaVelocity = targetVelocity - velocity;

            //Limit velocity change per update
            if (deltaVelocity.sqrMagnitude > maxDelta * maxDelta)
            {
                deltaVelocity.Normalize();
                deltaVelocity *= maxDelta;
            }

            deltaVelocity.y = 0f;

            rigidBody.AddForce(deltaVelocity, ForceMode.VelocityChange);
        }
    }

    float RelativeVectorMagnitude(Vector3 from, Vector3 to)
    {
        Vector3 projection = Vector3.Project(from, to);
        float size = projection.magnitude;
        size *= Mathf.Sign(Vector3.Dot(projection, to));
        return size;
    }
}
