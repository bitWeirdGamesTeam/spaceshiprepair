﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRotator : MonoBehaviour {

    public Transform model;
    //public SimpleCharacterMovement3D movement;

    public float turnSpeed = 5f;
    //public float forwardTilt = 10f;
    //public float sidewaysTilt = 10f;
    //public float tiltSpeed = 10f;

    private Vector2 controlAxis = Vector2.zero;
    private Quaternion rotationSmoothed;

    private Vector3 referenceForward = Vector3.forward;
    private Vector3 referenceRight = Vector3.right;
    private Vector3 targetDirection = Vector3.forward;
    private Vector3 targetDirectionRight = Vector3.right;

    public Vector2 ControlAxis
    {
        get{return controlAxis;}
        set{controlAxis = value;}
    }

    public float SetReferenceRotation
    {
        set
        {
            Quaternion refRotation = Quaternion.Euler(new Vector3(0f, value));
            referenceForward = refRotation * Vector3.forward;
            referenceRight = refRotation * Vector3.right;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateControls();
	}

    private void FixedUpdate()
    {
        UpdateModel();
    }

    void UpdateControls()
    {
        //controlAxis.x = Input.GetAxis("Horizontal");
        //controlAxis.y = Input.GetAxis("Vertical");

        if (controlAxis.sqrMagnitude > 0f)
        {
            targetDirection = referenceRight * controlAxis.x + referenceForward * controlAxis.y;
            targetDirection.Normalize();

            targetDirectionRight.x = targetDirection.z;
            targetDirectionRight.z = -targetDirection.x;
        }
    }

    void UpdateModel()
    {
        //float deltaY = -model.transform.rotation.eulerAngles.y;
        if (controlAxis.sqrMagnitude > 0f)
        {
            rotationSmoothed = Quaternion.Lerp(model.transform.rotation, Quaternion.LookRotation(targetDirection), turnSpeed * Time.deltaTime);
            model.transform.rotation = rotationSmoothed;
        }

        /*
        deltaY += model.transform.rotation.eulerAngles.y;

        float velocityForward = Vector3.Dot(movement.Velocity, targetDirection);
        //float sidewaysDot = Vector3.Dot(targetDirectionRight, model.transform.forward);
        float sidewaysDot = -deltaY / 10f;

        Vector3 targetTilt = model.transform.localRotation.eulerAngles;
        targetTilt.x = (velocityForward / movement.walkingSpeed) * forwardTilt;
        targetTilt.z = sidewaysDot * sidewaysTilt;// * controlAxis.magnitude;

        model.transform.localRotation = Quaternion.Lerp(model.transform.localRotation, Quaternion.Euler(targetTilt), tiltSpeed * Time.deltaTime);

        Debug.Log(targetTilt);
        */
    }
}
