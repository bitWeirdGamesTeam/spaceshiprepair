﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementControl : MonoBehaviour {

    public string horMovementAxisName = "Horizontal";
    public string verMovementAxisName = "Vertical";
    public string dashButton = "Dash";

    public SimpleCharacterMovement3D movement;
    public CharacterRotator rotator;

    private Vector3 controlAxisMovement;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateControls();
    }

    void UpdateControls()
    {
        controlAxisMovement.x = Input.GetAxis(horMovementAxisName);
        controlAxisMovement.y = Input.GetAxis(verMovementAxisName);

        movement.ControlAxis = controlAxisMovement;
        movement.Dash = Input.GetButton(dashButton);

        rotator.ControlAxis = controlAxisMovement;
    }
}
