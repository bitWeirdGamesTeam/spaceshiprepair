﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractChargeControl : MonoBehaviour {

    public string buttonName = "";
    public float tapRegisterDuration = 0.25f;
    [Range(0f, 1f)]
    public float minCharge = 0.1f;
    public float maxChargeTime = 3f;

    protected float charge = 0f;
    protected float tapTimer = 0f;
    protected float chargeTimer = 0f;
    
    public float Charge
    {
        get { return chargeTimer / maxChargeTime; }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        UpdateControls();
    }

    protected void UpdateControls()
    {
        UpdateButton();
        UpdateCharge();
    }

    protected void UpdateButton()
    {
        if (Input.GetButtonDown(buttonName))
            OnButtonDown();
        if (Input.GetButtonUp(buttonName))
            OnButtonUp();
    }

    protected void UpdateCharge()
    {
        if (Input.GetButton(buttonName))
        {
            if (tapTimer < tapRegisterDuration)
                tapTimer += Time.deltaTime;
            else
            {
                chargeTimer += Time.deltaTime;
                chargeTimer = Mathf.Clamp(chargeTimer, 0f, maxChargeTime);
                charge = minCharge + ((chargeTimer / maxChargeTime) * (1f - minCharge));
            }
        }
        else
        {
            if (tapTimer > 0 && tapTimer < tapRegisterDuration)
                OnClick();
            else
            {
                if (chargeTimer > 0f)
                    OnChargeRelease();
                charge = 0f;
            }
            chargeTimer = 0f;
            tapTimer = 0f;
        }
    }

    public void ResetCharge()
    {
        charge = 0f;
        chargeTimer = 0f;
        tapTimer = 0f;
    }

    protected abstract void OnButtonDown();
    protected abstract void OnButtonUp();
    protected abstract void OnClick();
    protected abstract void OnChargeRelease();
}
