﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManipulatorControl2 : MonoBehaviour
{
    public ObjectManipulator manipulator;

    public float throwSpeed = 10f;

    public string useButton = "";
    public string pickupDropButton = "";
    public string throwButton = "";

    private void Update()
    {
        if(Input.GetButtonDown(pickupDropButton))
        {
            if (manipulator.IsCarrying)
                manipulator.Drop();
            else
                manipulator.PickUp();
        }

        if (Input.GetButtonDown(throwButton))
        {
            if(manipulator.IsCarrying)
                manipulator.Throw(throwSpeed);
        }

        if(Input.GetButtonDown(useButton))
        {
            manipulator.Use();
        }
    }
}
