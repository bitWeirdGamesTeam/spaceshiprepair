﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShipBehaviour : MonoBehaviour
{
    public int lightYears;
    public int speedPerThruster = 1;
    public HealthBehaviour health;
    public List<ThrusterBehaviour> thrusters;

    public UnityEventInt OnLightYearsUpdated;
    public UnityEvent OnWin;

    public AudioClip damageClip;
    public AudioSource audioSource;

    private void OnEnable()
    {
        StartFlying();
    }

    public void StartFlying()
    {
        StartCoroutine(Fly());
    }

    public void StopFlying()
    {
        StopAllCoroutines();
    }

    IEnumerator Fly()
    {
        lightYears = 1000;

        while(lightYears > 0)
        {
            if (health.Health > 0)
            {
                for (int i = 0; i < thrusters.Count; i++)
                {
                    if (thrusters[i].CheckThrusterActivity())
                    {
                        lightYears -= speedPerThruster;
                        lightYears = Mathf.Max(0, lightYears);
                    }
                }
             
            }
            
            OnLightYearsUpdated.Invoke(lightYears);
            yield return new WaitForSeconds(0.5f);
        }

        OnWin.Invoke();
    }

    public void OnHealthChange()
    {

    }
}
