﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PickUpObject : MonoBehaviour {

    public Vector3 carryPoint;

    public Rigidbody body;

    public UnityEvent OnPickedUp;
    public UnityEvent OnDropped;
    public UnityEvent OnThrown;

    public HighlightEffect highlightEffect;

    public bool debug = false;

    private bool beingCarried = false;
    private Vector3 throwOrigin = Vector3.zero;

    private int transmitterOriginalLayer = 0;

    public Vector3 ThrowOrigin
    {
        get { return throwOrigin; }
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	}

    public void OnPickup()
    {
        beingCarried = true;

        if(OnPickedUp != null)
            OnPickedUp.Invoke();
    }

    public void OnThrow()
    {
        throwOrigin = transform.position;
        body = GetComponent<Rigidbody>();
        beingCarried = false;

        if (OnThrown != null)
            OnThrown.Invoke();
    }

    public void OnDrop()
    {
        body = GetComponent<Rigidbody>();
        beingCarried = false;

        if (OnDropped != null)
            OnDropped.Invoke();
    }

    private void OnDrawGizmos()
    {
        if(debug)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(throwOrigin, 0.1f);

            Gizmos.color = Color.white;
            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Gizmos.DrawSphere(carryPoint, 0.1f);
        }
    }
}
