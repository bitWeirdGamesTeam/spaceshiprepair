﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractUsable : MonoBehaviour {

    protected bool active = false;

    public bool toggle = false;
    public HighlightEffect highlightEffect;

    public void Toggle(Transform user)
    {
        if(toggle)
        {
            if (active == false)
                Activate(user);
            else
                Deactivate(user);
        }
        else
        {
            Activate(user);
            active = false;
        }
    }
    public void Activate(Transform user)
    {
        active = true;
        OnActivate(user);
    }
    public void Deactivate(Transform user)
    {
        active = false;
        OnDeactivate(user);
    }

    protected abstract void OnActivate(Transform user);
    protected abstract void OnDeactivate(Transform user);
}
