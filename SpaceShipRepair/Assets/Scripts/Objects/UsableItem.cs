﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UsableItem : MonoBehaviour
{
    public ItemType itemType;
    public bool isConsumable;
    public UnityEvent OnItemUsed;

    public void Used(bool used)
    {
        if (isConsumable && used)
            OnItemUsed.Invoke();
    }
}
