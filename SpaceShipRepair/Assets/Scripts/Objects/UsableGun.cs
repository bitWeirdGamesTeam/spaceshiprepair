﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsableGun : AbstractUsable
{
    protected override void OnActivate(Transform user)
    {
        GetComponent<AbstractGun>().fireControl = true;
    }

    protected override void OnDeactivate(Transform user)
    {
        GetComponent<AbstractGun>().fireControl = false;
    }
}
