﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RigidbodyData
{
    public float mass;
    public float drag;
    public float angularDrag;
    public bool useGravity;
    public bool isKinematic;
    public RigidbodyInterpolation interpolation;
    public CollisionDetectionMode collisionDetection;
    public RigidbodyConstraints constraints;

    public void Set(Rigidbody body)
    {
        mass = body.mass;
        drag = body.drag;
        angularDrag = body.angularDrag;
        useGravity = body.useGravity;
        isKinematic = body.isKinematic;
        interpolation = body.interpolation;
        collisionDetection = body.collisionDetectionMode;
        constraints = body.constraints;
    }

    public void Get(Rigidbody body)
    {
        body.mass = mass;
        body.drag = drag;
        body.angularDrag = angularDrag;
        body.useGravity = useGravity;
        body.isKinematic = isKinematic;
        body.interpolation = interpolation;
        body.collisionDetectionMode = collisionDetection;
        body.constraints = constraints;
    }
}

public class ObjectManipulator : MonoBehaviour {

    public Vector3 checkOffset;
    public Vector3 checkHalfExtents;
    public GameObject highlighter;
    public bool autoCheck = false;
    public bool highlight = false;
    public bool debug = false;

    [Header("Pickup & throw")]
    public Rigidbody userBody;
    public PickUpObject heldObject;
    public Vector3 carryPoint;
    public float throwVerticalSpeed = 1f;
    public LayerMask pickupLayers;
    private RigidbodyData oldBodyData;
    public bool heldObjectIsManipulable;

    [Header("Usage")]
    public bool useOnlyHeldObjects = false;
    public LayerMask useableLayers;

    public PickUpObject closestPickupObject;
    public AbstractUsable closestUsableObject;

    public bool IsCarrying
    {
        get
        {
            if (heldObject != null)
                return true;
            return false;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(autoCheck)
            AutoCheck();
	}

    private void AutoCheck()
    {
        PickUpObject pickUpObject = null;
        AbstractUsable usableObject = null;

        if (IsCarrying == false || IsCarrying == true && heldObjectIsManipulable == false)
        {
            pickUpObject = FindClosestPickUpObject();
            usableObject = FindClosestUsableObject();
            if (pickUpObject != null && usableObject != null && (pickUpObject.gameObject != usableObject.gameObject))
            {
                if (Vector3.SqrMagnitude(pickUpObject.transform.position - transform.position) < Vector3.SqrMagnitude(usableObject.transform.position - transform.position))
                    usableObject = null;
                else
                    pickUpObject = null;
            }
        }
        else
        {
            pickUpObject = null;
            usableObject = null;
        }

        if(highlight)
        {
            //Highlight objects here?
            //Look for highlight effects and enable/disable. Effect could be in pickup & usable scripts or on it's own

            //Disable highlight shit
            if (closestPickupObject != null && closestPickupObject.highlightEffect != null && pickUpObject != closestPickupObject)
                closestPickupObject.highlightEffect.SetHighlight(false);

            if (closestUsableObject != null && closestUsableObject.highlightEffect != null && usableObject != closestUsableObject)
                closestUsableObject.highlightEffect.SetHighlight(false);

            //Enable higlight shit
            if (pickUpObject != null)
            {
                if(pickUpObject.highlightEffect != null)
                    pickUpObject.highlightEffect.SetHighlight(true);
            }
            else if (usableObject != null)
            {
                if(usableObject.highlightEffect != null)
                    usableObject.highlightEffect.SetHighlight(true);
            }



            //PLACEHOLDER
            if(highlighter != null)
            {
                if (closestPickupObject != null)
                {
                    highlighter.SetActive(true);
                    highlighter.transform.position = closestPickupObject.transform.position;
                }
                else if (closestUsableObject != null)
                {
                    highlighter.SetActive(true);
                    highlighter.transform.position = closestUsableObject.transform.position;
                }
                else
                {
                    highlighter.SetActive(false);
                }
            }
        }

        closestPickupObject = pickUpObject;
        closestUsableObject = usableObject;
    }

    public void PickUp()
    {
        if (heldObject != null)
            return;

        if (autoCheck)
        {
            if (closestPickupObject != null)
                PickUpObject(closestPickupObject);
        }
        else
            PickUpObject(FindClosestPickUpObject());
    }

    public void PickUp(GameObject go)
    {
        if (heldObject != null)
            return;

        PickUpObject po = go.GetComponent<PickUpObject>();
        if (po != null)
        {
            PickUpObject(po);
        }
    }

    private PickUpObject FindClosestPickUpObject()
    {
        Collider[] colliders = CheckForColliders(pickupLayers);
        List<PickUpObject> pickUpList = FindPickUpsInColliders(colliders);
        return FindClosestInList<PickUpObject>(pickUpList);
    }

    private void PickUpObject(PickUpObject pickUpObject)
    {
        if (pickUpObject == null)
            return;

        SetHeldObject( pickUpObject);
        oldBodyData.Set(heldObject.body);
        Destroy(heldObject.body);
        heldObject.transform.parent = transform;
        heldObject.transform.rotation = transform.rotation;
        heldObject.transform.localPosition = carryPoint - heldObject.carryPoint;

        heldObject.OnPickup();
    }

    public void Drop()
    {
        if (heldObject == null)
            return;
        heldObject.transform.parent = null;
        Rigidbody body = heldObject.gameObject.AddComponent<Rigidbody>();
        heldObject.body = body;
        oldBodyData.Get(body);
        body.isKinematic = false;
        body.velocity = userBody.velocity;

        heldObject.OnDrop();
        //SetHeldObject(null);

        SetHeldObjectNull();
    }

    public void Throw(float speed)
    {
        if (heldObject == null)
            return;
        heldObject.transform.parent = null;
        Rigidbody body = heldObject.gameObject.AddComponent<Rigidbody>();
        heldObject.body = body;
        oldBodyData.Get(body);
        body.isKinematic = false;
        body.velocity = userBody.velocity + transform.forward * speed + Vector3.up * throwVerticalSpeed;

        heldObject.OnThrow();
        //SetHeldObject(null);
        SetHeldObjectNull();
    }

    public void Use()
    {
        AbstractUsable usable = null;

        //If holding an object try to use that one. If can't -> stop
        if (IsCarrying == true)
        {
            usable = heldObject.GetComponent<AbstractUsable>();
            if (usable != null)
            {
                usable.Toggle(transform);
                return;
            }
                
        }

        if (useOnlyHeldObjects == true)
            return;

        //Else check for nearby useable objects and use closest one
        if(autoCheck)
        {
            if (closestUsableObject != null)
                closestUsableObject.Toggle(transform);
        }
        else
        {
            usable = FindClosestUsableObject();
            if (usable != null)
                usable.Toggle(transform);
        }
    }

    public void SetHeldObject(PickUpObject obj)
    {
        
        heldObjectIsManipulable = false;
        if (obj != null)
        {
            heldObjectIsManipulable = obj.GetComponent<AbstractUsable>() != null;
            HealthBehaviour hp = obj.GetComponent<HealthBehaviour>();
            if (hp != null)
                hp.OnDeath.AddListener(SetHeldObjectNull);
        }
        heldObject = obj;
    }

    public void SetHeldObjectNull()
    {
        heldObjectIsManipulable = false;
        if (heldObject != null)
        {
            HealthBehaviour hp = heldObject.GetComponent<HealthBehaviour>();
            if (hp != null)
                hp.OnDeath.RemoveListener(SetHeldObjectNull);
            heldObject = null;
        }
    }

    private AbstractUsable FindClosestUsableObject()
    {
        Collider[] colliders = CheckForColliders(useableLayers);
        List<AbstractUsable> usableList = FindUsablesInColliders(colliders);
        return FindClosestInList<AbstractUsable>(usableList);
    }

    private Collider[] CheckForColliders(LayerMask layers)
    {
        return Physics.OverlapBox(transform.TransformPoint(checkOffset), checkHalfExtents, transform.rotation, layers.value);
    }

    private List<PickUpObject> FindPickUpsInColliders(Collider[] colliders)
    {
        List<PickUpObject> pickUpList = new List<PickUpObject>();
        foreach (Collider col in colliders)
        {
            if (col.attachedRigidbody != null)
            {
                PickUpObject pickUpObject = col.attachedRigidbody.GetComponent<PickUpObject>();
                if (pickUpObject != null)
                    pickUpList.Add(pickUpObject);
            }
        }
        return pickUpList;
    }

    private List<AbstractUsable> FindUsablesInColliders(Collider[] colliders)
    {
        List<AbstractUsable> usableList = new List<AbstractUsable>();
        foreach (Collider col in colliders)
        {
            if (col.attachedRigidbody != null)
            {
                AbstractUsable usableObject = col.attachedRigidbody.gameObject.GetComponent<AbstractUsable>();
                if (usableObject != null)
                    usableList.Add(usableObject);
            }
        }
        return usableList;
    }

    private T FindClosestInList<T>(List<T> list) where T : Component
    {
        if (list.Count > 0)
        {
            if (list.Count == 1)
                return list[0];
            else
            {
                T closestItem = null;
                float closestDistance = float.MaxValue;
                float distance = 0f;

                foreach (T item in list)
                {
                    distance = Vector3.SqrMagnitude(item.transform.position - transform.position);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestItem = item;
                    }
                }
                return closestItem;
            }
        }
        else
            return null;
    }

    private void OnDrawGizmos()
    {
        if (debug)
        {
            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Gizmos.DrawWireCube(checkOffset, checkHalfExtents * 2f);
            Gizmos.DrawSphere(carryPoint, 0.1f);
        }
    }
}
