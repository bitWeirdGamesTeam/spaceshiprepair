﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UsableObject : AbstractUsable {

    public UnityEventTransform OnActivateEvent;
    public UnityEventTransform OnDeactivateEvent;

    protected override void OnActivate(Transform user)
    {
        OnActivateEvent.Invoke(user);
    }

    protected override void OnDeactivate(Transform user)
    {
        OnDeactivateEvent.Invoke(user);
    }
}
