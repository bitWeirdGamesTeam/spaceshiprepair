﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ItemType : ScriptableObject
{
    public Sprite icon;
    public string name;
    public AudioClip impactClip;
}
