﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactNoiseBehaviour : MonoBehaviour {

    public float minTriggerSpeed = 1f;
    public Rigidbody rigidBody;
    public AudioClip impactNoise;
    public AnimationCurve speedVolumeCurve;
    public float soundDelay = 0.1f;

    private float timer = 0f;

    private void OnCollisionEnter(Collision collision)
    {
        //If body is lost attempt to find it
        if(rigidBody == null)
            rigidBody = GetComponent<Rigidbody>();

        if (rigidBody != null)
        {
            if (timer > 0f)
                timer -= Time.deltaTime;
            else
            {
                float speed = rigidBody.velocity.magnitude;
                if (speed >= minTriggerSpeed)
                {
                    //Play impactNoise, speedVolumeCurve.Evaluate(speed), 
                    timer = soundDelay;
                }
            }
        }
    }
}
