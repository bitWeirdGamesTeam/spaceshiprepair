﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightEffect : MonoBehaviour {

    public Outline outlineEffect;

    // Use this for initialization
    void Start()
    {
        outlineEffect.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetHighlight(bool active)
    {
        if (active)
            outlineEffect.enabled = true;
        else
            outlineEffect.enabled = false;
    }
}
