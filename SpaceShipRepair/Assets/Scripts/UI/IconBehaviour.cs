﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconBehaviour : MonoBehaviour
{
    public Image background;
    public Image icon;

    public void SetIcon(ItemType itemType)
    {
        bool isActive = itemType == null ? false : true;
        if (isActive)
            icon.sprite = itemType.icon;

        icon.gameObject.SetActive(isActive);
        background.gameObject.SetActive(isActive);
    }
}
