﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarBehaviour : MonoBehaviour
{
    public RectTransform background;
    public RectTransform fill;
    public float showRatio = 0.75f;

    public void UpdateBar(float ratio)
    {
        RectTransform rt = (transform as RectTransform);
        fill.sizeDelta = new Vector2(fill.sizeDelta.x, rt.sizeDelta.y * ratio);

        if(ratio < showRatio)
        {
            background.gameObject.SetActive(true);
            fill.gameObject.SetActive(true);
        }
        else
        {
            if(ratio >= 1f)
            {
                background.gameObject.SetActive(false);
                fill.gameObject.SetActive(false);
            }
        }
    }
}

