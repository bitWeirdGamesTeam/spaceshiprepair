﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldToScreenPosition : MonoBehaviour
{
    public Camera camera;
    public Transform worldPosition;
    public Vector3 offset;

    void Update()
    {
        SetPosition();

    }

    [ContextMenu("Set Position")]
    public void SetPosition()
    {
        Vector3 nameLabelPos = camera.WorldToScreenPoint(worldPosition.position);
        transform.position = nameLabelPos + offset;
    }
}
