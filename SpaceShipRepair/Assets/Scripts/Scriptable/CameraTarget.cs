﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    public Transform targetTransform;

    public CameraTargetsList targetList;

    public bool addOnEnable = true;

    private bool isTarget = false;

    public bool IsTarget
    {
        get { return isTarget; }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (targetTransform == null)
            targetTransform = transform;

        if (addOnEnable)
        {
            targetList.AddTarget(targetTransform);
            isTarget = true;
        }
    }

    public void AddTarget()
    {
        targetList.AddTarget(targetTransform);
        isTarget = true;
    }

    public void RemoveTarget()
    {
        targetList.RemoveTarget(targetTransform);
        isTarget = false;
    }
}
