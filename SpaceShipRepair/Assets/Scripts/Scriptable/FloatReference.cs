﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FloatReference{

    public bool useConstant = true;
    public float constantValue;
    public FloatVariable scriptableValue;

    public float Value
    {
        set
        {
            if (useConstant)
                constantValue = value;
            else
                scriptableValue.SetValue(value);
        }
        get { return useConstant ? constantValue : scriptableValue.value; }
    }

    public FloatReference()
    {
        useConstant = true;
    }

    public FloatReference(float value)
    {
        useConstant = true;
        constantValue = value;
    }

    public static implicit operator int(FloatReference reference)
    {
        return (int)reference.Value;
    }
}
