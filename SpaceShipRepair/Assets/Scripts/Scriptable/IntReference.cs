﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class IntReference
{
    public bool useConstant = true;
    public int constantValue;
    public IntVariable scriptableValue;

    public int Value
    {
        set
        {
            if (useConstant)
                constantValue = value;
            else
                scriptableValue.SetValue(value);
        }
        get { return useConstant ? constantValue : scriptableValue.value; }
    }

    public IntReference()
    {
        useConstant = true;
    }

    public IntReference(int value)
    {
        useConstant = true;
        constantValue = value;
    }

    public static implicit operator float(IntReference reference)
    {
        return reference.Value;
    }
}