﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FloatVariable : ScriptableObject
{
#if UNITY_EDITOR
    [Multiline]
    public string Description = "";
#endif

    public float value;

    public void SetValue(float newValue)
    {
        value = newValue;
    }

    public void SetValue(FloatVariable newValue)
    {
        value = newValue.value;
    }

    public void AddValue(float newValue)
    {
        value += newValue;
    }

    public void AddValue(FloatVariable newValue)
    {
        value += newValue.value;
    }
}
