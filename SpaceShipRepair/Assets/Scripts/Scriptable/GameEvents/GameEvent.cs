﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events/Game Event")]
public class GameEvent : ScriptableObject {

    public List<GameEventListener> listeners = new List<GameEventListener>();

    public void Trigger()
    {
        for(int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventTriggered();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        if (listeners.Contains(listener))
            return;
        listeners.Add(listener);
    }

    public void UnregisterListener(GameEventListener listener)
    {
        listeners.Remove(listener);
    }
}
