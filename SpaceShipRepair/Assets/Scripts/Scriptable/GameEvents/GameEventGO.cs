﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events/Game Event GameObject")]
public class GameEventGO : ScriptableObject
{

    private List<GameEventGOListener> listeners = new List<GameEventGOListener>();

    public void Trigger(GameObject value)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventTriggered(value);
        }
    }

    public void AddListener(GameEventGOListener listener)
    {
        listeners.Add(listener);
    }

    public void RemoveListener(GameEventGOListener listener)
    {
        listeners.Remove(listener);
    }
}
