﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventGOListener : MonoBehaviour {

    public GameEventGO gameEvent;
    public UnityEngine.Events.UnityEventGO responce;

    private void OnEnable()
    {
        if (gameEvent != null)
            gameEvent.AddListener(this);
    }

    private void OnDisable()
    {
        if (gameEvent != null)
            gameEvent.RemoveListener(this);
    }

    public void OnEventTriggered(GameObject value)
    {
        responce.Invoke(value);
    }
}
