﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CameraTargetsList : ScriptableObject
{
    public List<Transform> targets;

    public bool HasTargets
    {
        get
        {
            return targets.Count > 0;
        }
    }

    public void ClearTargets()
    {
        targets.Clear();
    }

    public void AddTarget(Transform target)
    {
        if (targets.Contains(target))
            return;
        targets.Add(target);
    }

    public void RemoveTarget(Transform target)
    {
        targets.Remove(target);
    }

    public Vector3 AveragePosition()
    {
        Vector3 position = Vector3.zero;

        if (targets.Count > 0)
        {
            int count = 0;
            foreach (Transform t in targets)
            {
                if (t == null)
                    continue;

                if (t.gameObject.activeInHierarchy)
                {
                    position += t.position;
                    count++;
                }
            }
            if (count > 0)
            {
                position /= count;
            }
        }
        return position;
    }
}
