﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class IntVariable : ScriptableObject
{
#if UNITY_EDITOR
    [Multiline]
    public string Description = "";
#endif

    public int value;

    public void SetValue(int newValue)
    {
        value = newValue;
    }

    public void SetValue(IntVariable newValue)
    {
        value = newValue.value;
    }

    public void AddValue(int newValue)
    {
        value += newValue;
    }

    public void AddValue(IntVariable newValue)
    {
        value += newValue.value;
    }
}