﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Repairer : MonoBehaviour
{
    public int repairPerTick = 1;
    public HealthBehaviour healthBehaviour;
    public List<ItemType> possibleItems;
    public UnityEventItemType OnSetRepairItem;
    public AudioSource audioSource;

    [SerializeField] private ItemType currentItem;
    private int currentRepairAmount;

    private void Start()
    {
        OnSetRepairItem.Invoke(null);
    }

    //public void DoRepair(ItemType item)
    //{
    //    if(currentItem == item)
    //        healthBehaviour.ChangeHealth(repairPerTick);
    //}

    public void DoRepair()
    {
        DoRepair(null);
    }

    public void DoRepair(Transform user)
    {
        if (user == null)
            return;

        ObjectManipulator manipulator = user.GetComponent<ObjectManipulator>();
        if(manipulator.heldObject != null)
        {
            UsableItem usableItem = manipulator.heldObject.GetComponent<UsableItem>();
            if(usableItem != null)
            {
                bool used = AttemptRepair(usableItem.itemType);
                usableItem.Used(used);
            }
        }

        if (currentRepairAmount >= 16)
            SetRandomRepairItem();
    }

    bool AttemptRepair(ItemType itemType)
    {
        if (currentItem == itemType)
        {
            healthBehaviour.ChangeHealth(repairPerTick);
            currentRepairAmount++;
            if(currentItem != null)
                audioSource.PlayOneShot(currentItem.impactClip);
            return true;
        }
        return false;

    }

    public void SetRandomRepairItem()
    {
        if (possibleItems.Count > 0)
        {
            int i = (int)Mathf.Repeat(possibleItems.IndexOf(currentItem) + Random.Range(1, possibleItems.Count), possibleItems.Count);
            
            SetRepairItem(possibleItems[i]);
        }
        else
            SetRepairItem(null);
    }

    public void SetRepairItem(ItemType itemType)
    {
        currentItem = itemType;
        currentRepairAmount = 0;

        OnSetRepairItem.Invoke(currentItem);
    }

    public void CheckRepairNeed(float ratio)
    {
        if (ratio >= 1f)
            SetRepairItem(null);
        if (ratio < 0.80f && currentItem == null)
            SetRandomRepairItem();
    }
}
