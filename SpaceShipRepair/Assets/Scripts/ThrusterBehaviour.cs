﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrusterBehaviour : MonoBehaviour, IEnergized
{
    public bool IsEnergized { get; set; } = true;
    public HealthBehaviour health;

    public bool CheckThrusterActivity()
    {
        return IsEnergized && health.Health > 0;
    }
}
