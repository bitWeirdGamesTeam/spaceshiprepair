﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {

    public float newtonsThirdLawCoeff = 1f;

    public void OnTrigger(Collider other)
    {
        Vector3 velocity = GetComponent<Rigidbody>().velocity;
        if (other.attachedRigidbody != null)
            other.attachedRigidbody.AddForce(velocity * newtonsThirdLawCoeff, ForceMode.VelocityChange);
        if (other.GetComponent<UnityEngine.AI.NavMeshAgent>() != null)
            other.GetComponent<UnityEngine.AI.NavMeshAgent>().velocity = velocity * newtonsThirdLawCoeff;

        GetComponent<DamageApplier>().ApplyDamage(other);
        GetComponent<AutoDisabler>().Disable();
    }
}
