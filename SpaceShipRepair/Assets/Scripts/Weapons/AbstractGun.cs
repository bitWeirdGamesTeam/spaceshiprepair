﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class AbstractGun : MonoBehaviour {

    public bool fireControl = false;
    public float roundsPerMinute = 100f;
    public int ammo = -1;

    public Transform muzzle;  //Point from where shots are fired along z-axis
    public ParticleSystem muzzleFlameParticles;

    public AudioClip fireStartSound;
    public AudioClip fireSound;
    public AudioClip fireEndSound;
    public AudioSource audioSource;

    public UnityEventInt OnAmmoChanged;

    private float fireTimer = 0f;
    private bool previousFire = false;

    private bool firing = false;

    protected float FiringDelay
    {
        get
        {
            if (roundsPerMinute > 0f)
                return 1f / (roundsPerMinute / 60f);
            else
                return float.MaxValue;
        }
    }
	
	// Update is called once per frame
	void Update () {
        UpdateFire();
    }

    public void AddAmmo(int amount)
    {
        SetAmmo(ammo + amount);
    }

    public void SetAmmo(int amount)
    {
        ammo = amount;
    }

    protected void UpdateFire()
    {
        if (fireControl == true && ammo != 0)
        {
            if(firing == false)
            {
                firing = true;
                FireStart();
            }

            FireHeld();

            //Jos ei käytetä sarjatulta
            if(roundsPerMinute <= 0f)
            {
                if (previousFire == false)
                    Shoot();
            }

            //Jos käytetään sarjatulta
            else
            {
                fireTimer -= Time.deltaTime;
                    
                if (fireTimer <= 0f)
                {
                    Shoot();
                    fireTimer = FiringDelay;
                }
            }
        }
        else
        {
            if(firing == true)
            {
                firing = false;
                fireTimer = 0f;
                StopSound();
                FireStop();
            }
        }

        previousFire = fireControl;
    }

    public void Shoot()
    {
        if(ammo != 0)

        FireProjectile();
        PlaySound();
        PlayParticles();

        if(ammo > 0)
            ammo--;

        if (OnAmmoChanged != null)
            OnAmmoChanged.Invoke(ammo);
    }

    protected void PlaySound()
    {
        if(audioSource != null)
        {
            if (fireSound != null)
                audioSource.PlayOneShot(fireSound);
            else if(audioSource.isPlaying == false)
            {
                audioSource.Play();
                if (fireStartSound != null)
                    audioSource.PlayOneShot(fireStartSound);
            }
        }
    }

    protected void StopSound()
    {
        if (audioSource != null)
        {
            audioSource.Stop();
        }
    }

    protected void PlayParticles()
    {
        if(muzzleFlameParticles != null)
        {
            muzzleFlameParticles.Play();
        }
    }

    abstract protected void FireProjectile();
    abstract protected void FireStart();
    abstract protected void FireHeld();
    abstract protected void FireStop();
}
