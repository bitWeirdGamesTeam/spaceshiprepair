﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipUi : MonoBehaviour
{
    public Text lightYears;
    public Text health;

    public void SetLightYears(int ly)
    {
        lightYears.text = "To Home: " + ly + " Light-Years";
    }

    public void SetShipHealth(float ratio)
    {
        health.text = "Ship Health: " + (int)(ratio * 100f) + "%";
    }
}
