﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public float delay = 1f;
    public UnityEvent OnTimerTick;
    public bool IsOn { get; private set; }


    private void OnEnable()
    {
        StartTimer();
    }

    private void OnDisable()
    {
        StopTimer();
    }

    public void StartTimer()
    {
        StartCoroutine(RunTimer());
    }

    public void StopTimer()
    {
        StopAllCoroutines();
        IsOn = false;
    }


    IEnumerator RunTimer()
    {
        float time = 0f;
        IsOn = true;
        while (IsOn)
        {
            yield return null;
            time += Time.deltaTime;
            if (time > delay)
            {
                OnTimerTick.Invoke();
                time -= delay;
            }
        }
    }
}
